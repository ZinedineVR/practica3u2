/* Código de JavaScript */

// Funcion calcular

function calcular(){
    valorAuto = document.getElementById('txtValor').value;
    enganche = document.getElementById('txtEnganche');
    totalFin = document.getElementById('txtFinanciar');
    planCredito = document.getElementById('cmbPlanes').value;
    pagoMensual = document.getElementById('txtPago');

    // operacion para calcular el enganche
    enganche.innerHTML = "$" + valorAuto * 0.30;
    
    // resultado del valor original menos el enganche
    resOp = valorAuto - (valorAuto * 0.30);

    // operacion para calcular el credito por plan
    if(planCredito == 12){
        totalFin.innerHTML = "$" + (resOp + (resOp * 0.125));
        pagoMensual.innerHTML = "$" + ((resOp + (resOp * 0.125)) / planCredito).toFixed(2);
    }else if(planCredito == 18){
        totalFin.innerHTML = "$" + (resOp + (resOp * 0.172));
        pagoMensual.innerHTML = "$" + ((resOp + (resOp * 0.172)) / planCredito).toFixed(2);
    }else if(planCredito == 24){
        totalFin.innerHTML = "$" + (resOp + (resOp * 0.21));
        pagoMensual.innerHTML = "$" + ((resOp + (resOp * 0.21)) / planCredito).toFixed(2);
    }else if(planCredito == 36){
        totalFin.innerHTML = "$" + (resOp + (resOp * 0.26));
        pagoMensual.innerHTML = "$" + ((resOp + (resOp * 0.26)) / planCredito).toFixed(2);
    }else if(planCredito == 48){
        totalFin.innerHTML = "$" + (resOp + (resOp * 0.45));
        pagoMensual.innerHTML = "$" + ((resOp + (resOp * 0.45)) / planCredito).toFixed(2);
    }

    
}

//Funcion Limpiar

function limpiar(){
    valor = document.getElementById('txtValor');
    enganche = document.getElementById('txtEnganche');
    totalFin = document.getElementById('txtFinanciar');
    pagoMensual = document.getElementById('txtPago');

    valor.value = "";
    enganche.innerHTML = "";
    totalFin.innerHTML = "";
    pagoMensual.innerHTML = "";
}